# Projeto de Grafos
Projeto correspondente ao trabalho final da disciplina de Grafos, do Curso de Sistemas de Informação da UFPA, ministrada pelo professor Nelson Cruz Sampaio Neto.
## Problema Proposto
O grupo de até 5 (cinco) integrantes deverá implementar um programa computacional, em linguagem de programação a sua escolha, que trabalhe os itens abaixo ao receber um grafo orientado ou não orientado como entrada.
<ol>
    <li> Verificar a existência de uma determinada aresta.</li>
    <li> Informar o grau de um dado vértice.</li>
    <li> Informar a adjacência de um dado vértice.</li>
    <li> Verificar se o grafo é cíclico.</li>
    <li> Verificar se o grafo é conexo.</li>
    <li> Informar quantos e quais são os componentes fortemente conexos do grafo.</li>
    <li> Verificar se o grafo é euleriano.</li>
    <li> Encontrar o caminho mais curto entre dois vértices. Se o grafo for valorado, o problema deve ser resolvido com o algoritmo de Dijkstra.</li>
    <li> Encontrar a árvore geradora mínima (AGM) do grafo.</li>
</ol>

OBS: Entrega até dia 03/07/2019

## Grupo
* Edmilton Pinto Peixeira
* George Alan Kardec Monteiro de Jesus
* Hana Gabrielle dos Santos Barata
* Pedro Paulo Lisboa de Sousa
* Victor Hugo Azevedo Ferreira